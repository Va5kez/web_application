class AddLoanRef < ActiveRecord::Migration[5.0]
  def change
    add_reference :loans, :employee, foreign_key: true
  end
end
